﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quiz
{
    public string texto;
}

public class ClassResposta : Quiz
{
    public bool ecorreta;
    
    public ClassResposta(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;
    }
}

public class ClassPergunta : Quiz
{ // CLASS PERGUNTAS
    public int pontos;
    public ClassResposta[] respostas;

    // METODO É PARA CRIAR AS PERGUNTAS
    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;
    }

    // METODO PARA ADD RESPOSTA
    public void AddResposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassResposta[4];
        }

        for (int i = 0; i < respostas.Length; i++)
        {
            if (respostas[i] == null)
            {
                respostas[i] = new ClassResposta(a, b);
                break;
            }
        }
    }

    // LISTA ou EXIBIR as respostas
    public void ListarRespostas()
    {
        if (respostas != null && respostas.Length > 0)
        {
            foreach (ClassResposta r in respostas)
            {
                Debug.Log(r);
            }
        }
    }

    // Checar resposta correta
    public bool ChecarRespostaCorreta(int pos)
    {
        if (respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }

} // FIM CLASS PERGUNTAS


// CLASS LÓGICA - PRINCIPAL
public class Logica : MonoBehaviour
{ // INICIO CLASS LOGICA

    public Text tituloPergunta;
    public Button[] respostaBtn = new Button[4];
    public List<ClassPergunta> PerguntasLista = new List<ClassPergunta>();

    private ClassPergunta perguntaAtual;

    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinal;
    private int contagemPerguntas = 0;
    private float tempo;
    private bool carregarProximaPergunta;
    private int pontos = 0;


    void Start()
    { // METODO START

        ClassPergunta p1 = new ClassPergunta("1. De quem é esse famoso bordão: “Erooooooooooooou” ?", 1);
        p1.AddResposta("Fausto Silva (Faustão)", true);
        p1.AddResposta("Silvio Santos", false);
        p1.AddResposta("José Luiz Datena", false);
        p1.AddResposta("Sikêra Junior", false);

        ClassPergunta p2 = new ClassPergunta(" Complete a frase “Caraaai [........]” ", 2);
        p2.AddResposta("Astronauta", false);
        p2.AddResposta("Diogo", false);
        p2.AddResposta("Borracha", true);
        p2.AddResposta("Jennifer", false);

        ClassPergunta p3 = new ClassPergunta("3. pós descer, subir e rebolar, devemos:", 2);
        p3.AddResposta("Descansar", false);
        p3.AddResposta("Escolher outra música", false);
        p3.AddResposta("Fazer uma revolução", false);
        p3.AddResposta("Segurar o forninho", true);

        ClassPergunta p4 = new ClassPergunta("4. Quem foi que disse “Você que fuma maconha, que usa crack, você vai morrer daqui para o Natal” ?", 3);
        p4.AddResposta("Fausto Silva (Faustão)", false);
        p4.AddResposta("Silvio Santos", false);
        p4.AddResposta("José Luiz Datena.", false);
        p4.AddResposta("Sikêra Junior", true);

        ClassPergunta p5 = new ClassPergunta("5. Segundo Datena devemos dizer:", 1);
        p5.AddResposta("“Me dá viagens”", false);
        p5.AddResposta("“Me dá ibagens”", true);
        p5.AddResposta("“Me dá linhagens” ", false);
        p5.AddResposta("“Me dá o comandante Hamilton”", false);

        ClassPergunta p6 = new ClassPergunta("6. Aquele que não sabe de nada é:", 2);
        p6.AddResposta("John Snow", false);
        p6.AddResposta("Cumpadi Washington", false);
        p6.AddResposta("Inocente", true);
        p6.AddResposta("N.D.A", false);

        ClassPergunta p7 = new ClassPergunta("7. Depois de comprar o “Freeza” na “feirinha” e dar entrevista, você deve comer por:", 3);
        p7.AddResposta("Estar com fome", false);
        p7.AddResposta("Estar Cagado de fome", true);
        p7.AddResposta("Fazer a social", false);
        p7.AddResposta("Estar morrendo de fome", false);

        ClassPergunta p8 = new ClassPergunta("8. A pessoa filma e fala, logo ela é:", 5);
        p8.AddResposta("O bixão memo hein doido", true);
        p8.AddResposta("Técnico em Multimídia", false);
        p8.AddResposta("Repórter", false);
        p8.AddResposta("Alguém que não deseja se identificar", false);

        ClassPergunta p9 = new ClassPergunta("9. Depois de ser humilhado, levar uma grande surra moral, ser largado às traças no chão. Ao se levantar você deve:", 2);
        p9.AddResposta("Juntar seus cacos e ir embora", false);
        p9.AddResposta("Refletir sobre a lição", false);
        p9.AddResposta("Mudar de país", false);
        p9.AddResposta("Gritar: JÁ ACABOU JÉSSICA?", true);

        ClassPergunta p10 = new ClassPergunta("10. Quem fez a música “Rap dos Memes”?", 10);
        p10.AddResposta("Cauê Moura", true);
        p10.AddResposta("Jacaré Banguela", false);
        p10.AddResposta("Kibe Louco", false);
        p10.AddResposta("Felipe Neto", false);



        PerguntasLista.Add(p1);
        PerguntasLista.Add(p2);
        PerguntasLista.Add(p3);
        PerguntasLista.Add(p4);
        PerguntasLista.Add(p5);
        PerguntasLista.Add(p6);
        PerguntasLista.Add(p7);
        PerguntasLista.Add(p8);
        PerguntasLista.Add(p9);
        PerguntasLista.Add(p10);

        perguntaAtual = p1;

        ExibirPerguntasNoQuiz();


    } // FIM METODO START

    void Update()
    {
        if (carregarProximaPergunta == true)
        {
            tempo += Time.deltaTime;
            if (tempo > 3)
            {
                tempo = 0;
                carregarProximaPergunta = false;
                ProximaPergunta();
            }
        }
    }


    private void ExibirPerguntasNoQuiz()
    {
        tituloPergunta.text = perguntaAtual.texto;
        respostaBtn[0].GetComponentInChildren<Text>().text = perguntaAtual.respostas[0].texto;
        respostaBtn[1].GetComponentInChildren<Text>().text = perguntaAtual.respostas[1].texto;
        respostaBtn[2].GetComponentInChildren<Text>().text = perguntaAtual.respostas[2].texto;
        respostaBtn[3].GetComponentInChildren<Text>().text = perguntaAtual.respostas[3].texto;
    }

    public void ProximaPergunta()
    {
        contagemPerguntas++;
        if (contagemPerguntas < PerguntasLista.Count)
        {
            perguntaAtual = PerguntasLista[contagemPerguntas];
            ExibirPerguntasNoQuiz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PainelFinal.SetActive(true);
            PainelFinal.transform.GetComponentInChildren<Text>().text = "Pontos: " + pontos.ToString();
        }
    }

    public void ClickResposta(int pos)
    {
        if (perguntaAtual.respostas[pos].ecorreta)
        {
            //Debug.Log("Resposta certa!!!");
            PainelCorreto.SetActive(true);
            pontos += perguntaAtual.pontos;
                
        }
        else
        {
            //Debug.Log("Resposta errada!!!");
            PainelErrado.SetActive(true);
        }

        carregarProximaPergunta = true;
    }

    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void Credito()
    {
        SceneManager.LoadScene("Credito");
    }

    public void SairJogo()
    {
        Application.Quit();
    }




}// FIM CLASS LOGICA
